# ![FMO](https://gitlab.com/uploads/-/system/project/avatar/29705126/FMOmeter.png?width=64) FMO - FoundryVTT Media Optimizer 

A one stop-shop for getting your image and music files FoundryVTT-ready for the best web user experience.

[[_TOC_]]

## i. Objective

This is a learning exercise for the creation of a desktop .NET app and learning best practices for software design along the way.

Currently practicing
 - Version Control (Git/Gitlab)
 - .NET 5
 - C#
 - Requirements Identification & Documentation
 - Architecture Identification & Documentation
 - Test-Driven Development
 - CLI

## 1. Problem-Definition
There are too many steps and too many programs involved in getting a piece of media optimized for FoundryVTT following [FoundryVTT's Media Optimization Guide](https://foundryvtt.com/article/media/) for the best web-user experience.

'Optimized' here meaning resizing to the appropriate use-case (such as tokens or scene backdrops) and converting to the web friendly filetypes .webp and .ogg for images and sound, respectfully.

```mermaid
graph LR
A(("Source <br> Image File"))-->B["Image Editor<br>(e.g. GIMP )"];
B-->C(("Optimized <br> Image File"));
C-->D["File Transfer Application <br> (e.g. FileZilla)"];
D-->E[("FoundryVTT <br> Server")];
F(("Source <br> Sound File"))-->G["Audio Editor<br>(e.g. Audacity)"];
G-->H(("Optimized <br> Sound File"))-->D;
```

## [2. Functional Requirements](https://gitlab.com/SuzEngine/FMO/-/issues/4)
FoundryVTT Media Optimizer (FMO) will optimize media and store them in a given Foundry server for immediate use.

```mermaid
graph LR;
A(("Source <br> Image File"))-->B[Foundry<br>Media<br>Optimizer];
B-->C[("FoundryVTT <br> Server")];
D(("Source <br> Sound File"))-->B;
```


### 2.1 Input Formats

Typical source media have the following uses and characteristics.
 - Images 
   - Backgrounds, Battlemaps, Tokens, Avatars, etc.
   - .png, .jpeg, .bmp, etc.
   - variety of sizes/resolutions which can sometimes become warped when rendered into FoundryVTT
- Sound 
  - Music, Local Effects
  - .wav, .flac, etc.

### 2.2 Output Formats

FMO will convert inputs to web friendly filetypes (.webp/.ogg for images and sound, respectfully) and crop/resize them intelligently to preset specifications (such as 400x400 px for medium-sized tokens or 2048 × 1080 for a scene backdrop).  The user will be able to modify and add to these presets.

FMO will then deliver the optimized file to the desired location, typically either a local instance of FoundryVTT or a server where FoundryVTT is being hosted, and following FoundryVTT's recommended file structure.

First iteration of FMO will output its error/activity reports in a simple .txt file.


### 2.3 User Interface

#### UI Proposal A 

[Issue Link](https://gitlab.com/SuzEngine/FMO/-/issues/1) / [Prototype Link](https://www.figma.com/proto/rAooAaBXl8ouVv2vmluCH3/FMO-Mockup?page-id=0%3A1&node-id=13%3A43389&viewport=494%2C48%2C0.27&scaling=min-zoom&starting-point-node-id=7%3A134137) / [Minimum Viable Product](https://gitlab.com/SuzEngine/FMO/-/issues/2)

![Default View Mockup width="20"](resources/GitLab/UI_Mockups/Default_View.png "Image Tab/Default View UI Mockup")

#### Style/Element Standards

Currently following [WinUI 2.7](https://docs.microsoft.com/en-us/windows/apps/winui/winui2/release-notes/winui-2.7), to be considered again at a later date.

#### Shortcut Keys

To be defined after research.

#### Accommodations for visually impaired users.

To be defined after research.

### 2.4 Hardware Interfaces

Early versions will be made for use in a Windows 10 environment as a desktop application.

Testing and deployment in other environments will be researched and implemented at a later date.

### 2.5 Software Interfaces

#### Library Dependencies

- [Magick.NET](https://github.com/dlemstra/Magick.NET)
- [SSH.NET](https://github.com/sshnet/SSH.NET)
- [NAudio](https://github.com/naudio/NAudio)
- [NVorbis](https://github.com/NVorbis/NVorbis)
- [NAudio Vorbis Encoder](https://github.com/NAudio/Vorbis)

Research will be done to find a library/wrapper for ffmpeg which could replace the need for NAudio, NVorbis, and NAudio Vorbis Encoder. ([related issue](https://gitlab.com/SuzEngine/FMO/-/issues/7))

### 2.6 Communications Interfaces

To be defined after research before developing the server upload handler.

First iteration of FMO will focus on FTP servers and their related requirements (Host Key, User ID, User Password, Error Handling).

More knowledge regarding FTP required.

## [3. Non-functional Requirements](https://gitlab.com/SuzEngine/FMO/-/issues/3)

### 3.1 Expected Response Time

This is unfamiliar ground, so the following are estimates that are likely to change.

Most actions should be instantaneous (0.1 seconds) and larger actions, such as converting a large image file (over 2k resolution) should be ~1s.  Exceptionally large actions, such as converting an exceptionally large sound file (over 10 minutes in length) should take no longer than 10s and have a progess indicator to communicate to the user. Any longer should create a dialogue detailing the unusual processing time and give an option to cancel the action.

Upload to server is variable but should still take less than 10s.  Progress and steps in the upload process should be displayed to the user.  Built in timeout if progress doesn't advance (~30-60s)

### 3.2 Security

More research needed, section is likely to change.

Up through [Version 0.2 MVP](https://gitlab.com/SuzEngine/FMO/-/milestones/3) security will be simple authentication of the files being used.

This section will be updated with the necessary security requirements File Transfer Protocol (FTP) before work begins on [Version 0.3](https://gitlab.com/SuzEngine/FMO/-/milestones)

### 3.3 Reliability

FMO will follow Test-Driven Development best practices and create unit tests before or in tandem with each method written.  This should account for a majority of normal behaviors, but FMO will catch unexpected file types/sizes and prompt the user on how to proceed.

This section will have to be revisited before construction begins on [Version 0.3](https://gitlab.com/SuzEngine/FMO/-/milestones)

### 3.4 Minimum Memory/Disk Space Requirements

Unlikely to be an issue, but will measure during testing and create more exact specifications before [Version 1.0](https://gitlab.com/SuzEngine/FMO/-/milestones/8)

### 3.5 Maintainability

By following object-oriented design, FMO will be able to be modified easily to fulfill additional use-cases, such as being a web application, a command line executable, or add additional server protocols to its upload behavior.  The basic behaviors will be properly abstracted out and placed into interfaces.  Utilizing .NET 5 will greatly contribute to this end.

### 3.6 Success/Failure Definitions

FMO will succeed if it can take an image or audio file, convert it according to specifications, and upload it to a server.

Failure will be the inability to do any of the above or if the finish design does not allow for easy adaptations.

## 4. Architecture

### 4.1 Program Organization

#### 4.1.1 Subsystem View

```mermaid
flowchart TD
A["User Interface"]<-->B["Application<br> Level Classes"];
B-->C["Data Storage"];
B-->D[".NET Dependencies"];
```

#### 4.1.2 Class View

```mermaid
classDiagram

    IConvert <|-- Optimizer
    Optimizer <|-- ImageOptimizer
    Optimizer <|-- AudioOptimizer
    NAudio <.. AudioOptimizer
    NVorbisAndEncoder <.. AudioOptimizer
    MagickdotNET <.. ImageOptimizer
    IUpload <|-- ServerHandler
    ServerHandler <|-- SFTPHandler
    SSHdotNET <.. SFTPHandler
    PresetConfig .. Optimizer
    TempFolder .. Optimizer
    TempFolder .. ServerHandler
    PresetConfig .. ServerHandler

    class IConvert{
    convert()
    newName()
    saveTo()
    crop()
    <<Interface>>}

    class IUpload{
    <<Interface>>}

    class SSHdotNET{
    <<Library>>}
    
    class Optimizer{
        <<Abstract>>
        string sourceFilePath
        string targetFilePath
        const int MAXFILESIZE
    }

    class ImageOptimizer{
        int xResize
        int yResize
        int xSourceSize
        int ySourceSize
    }

    class MagickdotNET{
        MagickReadSettings()
        MagickImage()
        <<Library>>
    }

    class ServerHandler{
        <<Abstract>>
    }

    class SFTPHandler{
    }

    class NAudio{
    <<Library>>}

    class NVorbisAndEncoder{
    <<Library>>}
```
### 4.2 Data Design

PresetConfig's data, which will hold arrays that can be edited or added to, will be a sequential-access list, as there will only be a limited number of options and for ease of use.

The Optimizer and ServerHandler objects will trade the optimized media file via a temporary destination folder, which will be deleted after a successful upload (after prompting)

### 4.3 Resource Management

FMO's performance will be monitored to see if the actions are light enough to enable easy scalability ie handling a directory of images at once rather than individually.

### 4.4 Scalability 

Depending on performance, behaviors for handling large number of files may be added.  Practicing good arcchitecture and design should make FMO's convert-edit-and-deliver behavior may be easily changed to fulfill other use cases in the future.

### 4.5 Internationalization/Localization

Abstracting strings away from the production code into a resource file database will be investigated and likely attempted, but may prove outside the scope of this exercise.

## 5. Key Construction Decisions

### 5.1 Language Chosen

C# for backend, XAML for frontend because those are the dev team's (me) primary language.

The requirements document would have at least posited JavaScript otherwise, depending on the library availability.  FMO could be better suited as a FireFox plugin or a Foundry add on

### 5.2 Conventions

Any class part of the Optimizer hierarchy will have Optimizer as part of their name.
Any class part of the Handler hierarchy will have Handler as part of their name.

### 5.3 Technology Wave

FMO will utilize .NET 5.0 for stability and to experiment with other platforms.
